# README #

Welcome to Scrap Wikiart tiny rpoject!

### What is this repository for? ###

A Web scrap solution to get all the high-resolution images from one author, from Wikiart.org web site. Each author has from one to hundreds of pictures, with different resolutions. Scap Wikiart can determine which are the high-res ones, and prepare a list of picture image links to be downloaded later.

* Version 1.0

Initial release

### How do I get set up? ###

Python 3.x is required as is the following modules or libraries:

    * requests
    * requests_html
    * Beautifulsoup 4

Modules are available in PyP repository and you can install it with the following commands:

    *   pip install bs4
    *   pip install requests
    *   pip install requests_html

* Configuration

Only one thing you need to setup in the code. The constant PAGE_ARTIST must point to the folder part of the author home page URL, as is:
    PAGE_ARTIST = "salvador-dali/"

